import character.*;
import item.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ItemTest {
    @Test
    void equipWeapon_levelRequirementNotMet_shouldThrowInvalidWeaponException() {
        Weapon testAxe = new Weapon("Common Axe", 2, ItemSlot.weapon, WeaponType.axe, 15, 1);
        Warrior testWarrior = new Warrior("Test Character");
        assertThrows(InvalidWeaponException.class, () -> testWarrior.equipWeapon(testAxe));
    }

    @Test
    void equipArmor_levelRequirementNotMet_shouldThrowInvalidArmorException() {
        Armor testPlate = new Armor("Common Plate Body Armor", 2, ItemSlot.body, ArmorType.plate, 0, 2, 0);
        Warrior testWarrior = new Warrior("Test Character");
        assertThrows(InvalidArmorException.class, () -> testWarrior.equipArmor(testPlate));
    }

    @Test
    void equipWeapon_invalidWeaponType_shouldThrowInvalidWeaponException() {
        Weapon testBow = new Weapon("Common Bow", 1, ItemSlot.weapon, WeaponType.bow, 15, 3);
        Warrior testWarrior = new Warrior("Test Character");
        assertThrows(InvalidWeaponException.class, () -> testWarrior.equipWeapon(testBow));
    }

    @Test
    void equipArmor_invalidArmorType_shouldThrowInvalidArmorException() {
        Armor testCloth = new Armor("Common Cloth Head Armor", 1, ItemSlot.head, ArmorType.cloth, 2, 0, 5);
        Warrior testWarrior = new Warrior("Test Character");
        assertThrows(InvalidArmorException.class, () -> testWarrior.equipArmor(testCloth));
    }

    @Test
    void equipWeapon_validWeapon_shouldReturnTrue() throws InvalidWeaponException {
        Weapon testAxe = new Weapon("Common Axe", 1, ItemSlot.weapon, WeaponType.axe, 15, 1);
        Warrior testWarrior = new Warrior("Test Character");
        Boolean expected = true;
        Boolean actual = testWarrior.equipWeapon(testAxe);
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_validArmor_shouldReturnTrue() throws InvalidWeaponException, InvalidArmorException {
        Armor testPlate = new Armor("Common Plate Body Armor", 1, ItemSlot.body, ArmorType.plate, 0, 2, 0);
        Warrior testWarrior = new Warrior("Test Character");
        Boolean expected = true;
        Boolean actual = testWarrior.equipArmor(testPlate);
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPS_noWeapon_shouldReturnProperValue() {
        Warrior testWarrior = new Warrior("Test Character");
        double expectedDPS = 1*(1 + (5 / 100.0));
        double actualDPS = testWarrior.calculateDPS();
        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void calculateDPS_withWeapon_shouldReturnProperValue() throws InvalidWeaponException {
        Warrior testWarrior = new Warrior("Test Character");
        Weapon testAxe = new Weapon("Common Axe", 1, ItemSlot.weapon, WeaponType.axe, 7, 1.1);
        testWarrior.equipWeapon(testAxe);
        double expectedDPS = (7*1.1)*(1 + (5 / 100.0));
        double actualDPS = testWarrior.calculateDPS();
        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void calculateDPS_withWeaponAndArmor_shouldReturnProperValue() throws InvalidWeaponException, InvalidArmorException {
        Warrior testWarrior = new Warrior("Test Character");
        Weapon testAxe = new Weapon("Common Axe", 1, ItemSlot.weapon, WeaponType.axe, 7, 1.1);
        Armor testPlate = new Armor("Common Plate Body Armor", 1, ItemSlot.body, ArmorType.plate, 0, 1, 0);
        testWarrior.equipWeapon(testAxe);
        testWarrior.equipArmor(testPlate);
        double expectedDPS = (7*1.1)*(1 + ((5+1) / 100.0));
        double actualDPS = testWarrior.calculateDPS();
        assertEquals(expectedDPS, actualDPS);
    }
}
