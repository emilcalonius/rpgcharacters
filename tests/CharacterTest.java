import character.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CharacterTest {
    @Test
    void constructor_createNewCharacter_lvlShouldBeOne() {
        Mage testCharacter = new Mage("Test Character");
        int expectedLvl = 1;
        int actualLvl = testCharacter.getLvl();
        assertEquals(expectedLvl, actualLvl);
    }

    @Test
    void lvlUp_callMethodOnce_lvlShouldBeTwo() {
        Mage testCharacter = new Mage("Test Character");
        int expectedLvl = 2;
        testCharacter.lvlUp();
        int actualLvl = testCharacter.getLvl();
        assertEquals(expectedLvl, actualLvl);
    }

    @Test
    void constructor_createNewMage_shouldHaveProperAttributes() {
        Mage testCharacter = new Mage("Test Character");
        int expectedIntelligence = 8;
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void constructor_createNewRanger_shouldHaveProperAttributes() {
        Ranger testCharacter = new Ranger("Test Character");
        int expectedIntelligence = 1;
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void constructor_createNewRogue_shouldHaveProperAttributes() {
        Rogue testCharacter = new Rogue("Test Character");
        int expectedIntelligence = 1;
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void constructor_createNewWarrior_shouldHaveProperAttributes() {
        Warrior testCharacter = new Warrior("Test Character");
        int expectedIntelligence = 1;
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void lvlUp_callMethodOnceForMage_shouldHaveProperAttributes() {
        Mage testCharacter = new Mage("Test Character");
        int expectedIntelligence = 13;
        int expectedStrength = 2;
        int expectedDexterity = 2;
        testCharacter.lvlUp();
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void lvlUp_callMethodOnceForRanger_shouldHaveProperAttributes() {
        Ranger testCharacter = new Ranger("Test Character");
        int expectedIntelligence = 2;
        int expectedStrength = 2;
        int expectedDexterity = 12;
        testCharacter.lvlUp();
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void lvlUp_callMethodOnceForRogue_shouldHaveProperAttributes() {
        Rogue testCharacter = new Rogue("Test Character");
        int expectedIntelligence = 2;
        int expectedStrength = 3;
        int expectedDexterity = 10;
        testCharacter.lvlUp();
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }

    @Test
    void lvlUp_callMethodOnceForWarrior_shouldHaveProperAttributes() {
        Warrior testCharacter = new Warrior("Test Character");
        int expectedIntelligence = 2;
        int expectedStrength = 8;
        int expectedDexterity = 4;
        testCharacter.lvlUp();
        int actualIntelligence = testCharacter.getIntelligence();
        int actualStrength = testCharacter.getStrength();
        int actualDexterity = testCharacter.getDexterity();
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
    }
}