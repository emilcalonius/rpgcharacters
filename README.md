# RPG Characters

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A  Java application with a command-line interface for creating RPG characters.
Project done as part of Experis Academy full-stack graduate program.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Licence](#license)

## Background

This is a console application made as an assignment for Noroff Accelerate Java Full-Stack course.

The program aims to demonstrate skills with Java fundamentals and the understanding of object-oriented programming paradigm.

The project covered Java essential features (data structures, modularization, exception handling etc.), object-oriented programming and unit testing.

## Install

- Install the newest version of JDK
- Install Intellij
- Clone repository

## Usage
- Run the application

## Maintainers

[@emilcalonius](https://gitlab.com/emilcalonius)

## License

MIT © Emil Calonius

