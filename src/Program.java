import character.*;
import character.Character;
import character.*;
import item.*;

import java.util.HashMap;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n" +
                " _______  _______  _______    _______           _______  _______  _______  _______ _________ _______  _______  _______ \n" +
                "(  ____ )(  ____ )(  ____ \\  (  ____ \\|\\     /|(  ___  )(  ____ )(  ___  )(  ____ \\\\__   __/(  ____ \\(  ____ )(  ____ \\\n" +
                "| (    )|| (    )|| (    \\/  | (    \\/| )   ( || (   ) || (    )|| (   ) || (    \\/   ) (   | (    \\/| (    )|| (    \\/\n" +
                "| (____)|| (____)|| |        | |      | (___) || (___) || (____)|| (___) || |         | |   | (__    | (____)|| (_____ \n" +
                "|     __)|  _____)| | ____   | |      |  ___  ||  ___  ||     __)|  ___  || |         | |   |  __)   |     __)(_____  )\n" +
                "| (\\ (   | (      | | \\_  )  | |      | (   ) || (   ) || (\\ (   | (   ) || |         | |   | (      | (\\ (         ) |\n" +
                "| ) \\ \\__| )      | (___) |  | (____/\\| )   ( || )   ( || ) \\ \\__| )   ( || (____/\\   | |   | (____/\\| ) \\ \\__/\\____) |\n" +
                "|/   \\__/|/       (_______)  (_______/|/     \\||/     \\||/   \\__/|/     \\|(_______/   )_(   (_______/|/   \\__/\\_______)\n" +
                "                                                                                                                       \n");
        Boolean run = true;
        String command;
        HashMap<Integer, Character> characters = new HashMap<>();
        String name;

        // Create some equippable items
        HashMap<Integer, Item> items = new HashMap<>();
        items.put(1, new Weapon("Common Axe", 1, ItemSlot.weapon, WeaponType.axe, 15, 1));
        items.put(2, new Weapon("Common Sword", 1, ItemSlot.weapon, WeaponType.sword, 15, 1.5));
        items.put(3, new Weapon("Common Bow", 1, ItemSlot.weapon, WeaponType.bow, 10, 1));
        items.put(4, new Weapon("Common Dagger", 1, ItemSlot.weapon, WeaponType.dagger, 10, 2));
        items.put(5, new Weapon("Common Staff", 1, ItemSlot.weapon, WeaponType.staff, 30, 0.5));
        items.put(6, new Armor("Common Plate Body Armor", 1, ItemSlot.body, ArmorType.plate, 0, 2, 0));
        items.put(7, new Armor("Common Cloth Body Armor", 1, ItemSlot.body, ArmorType.cloth, 2, 0, 1));
        items.put(8, new Armor("Common Leather Body Armor", 1, ItemSlot.body, ArmorType.leather, 0, 0, 2));
        items.put(9, new Armor("Common Mail Body Armor", 1, ItemSlot.body, ArmorType.mail, 0, 1, 1));


        System.out.println("Commands: \n" +
                "character - create new character\n" +
                "lvlup - level up your character\n" +
                "equip - equip an item\n" +
                "stats - show character stats\n" +
                "quit - close the application\n");
        // Main loop of program, run until user gives command to exit
        while(run) {
            System.out.println("Please give a command: character/lvlup/equip/stats/quit");
            command = scanner.nextLine();
            switch (command) {
                case "character":
                    System.out.println("Choose class: mage/ranger/rogue/warrior");
                    command = scanner.nextLine();
                    switch(command) {
                        case "mage":
                            System.out.println("Please give a name:");
                            name = scanner.nextLine();
                            Mage mage = new Mage(name);
                            characters.put(1, mage);
                            break;
                        case "ranger":
                            System.out.println("Please give a name:");
                            name = scanner.nextLine();
                            Ranger ranger = new Ranger(name);
                            characters.put(1, ranger);
                            break;
                        case "rogue":
                            System.out.println("Please give a name:");
                            name = scanner.nextLine();
                            Rogue rogue = new Rogue(name);
                            characters.put(1, rogue);
                            break;
                        case "warrior":
                            System.out.println("Please give a name:");
                            name = scanner.nextLine();
                            Warrior warrior = new Warrior(name);
                            characters.put(1, warrior);
                            break;
                        default:
                            System.out.println("Not a character class.");
                            break;
                    }
                    break;
                case "lvlup":
                    if(characters.get(1) == null) {
                        System.out.println("No active character.");
                    } else {
                        characters.get(1).lvlUp();
                        System.out.println("Character lvl: " + characters.get(1).getLvl());
                    }
                    break;
                case "equip":
                    System.out.println("Choose item by typing its number:");
                    int i = 1;
                    for(Item item : items.values()) {
                        System.out.println(i + " " + item.getName() + " (Required level: " + item.getLvlReq() + ")");
                        i++;
                    }
                    int itemIndex = Integer.parseInt(scanner.nextLine());
                    if(items.get(itemIndex).getSlot().equals(ItemSlot.weapon)) {
                        try {
                            characters.get(1).equipWeapon((Weapon) items.get(itemIndex));
                        } catch(InvalidWeaponException e) {
                            System.out.println(e.getMessage());
                        }
                    } else {
                        try {
                            characters.get(1).equipArmor((Armor) items.get(itemIndex));
                        } catch(InvalidArmorException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    break;
                case "stats":
                    if(characters.get(1) == null) {
                        System.out.println("No active character.");
                    } else {
                        System.out.println(characters.get(1).displayStats());
                    }
                    break;
                case "quit":
                    run = false;
                    break;
                default:
                    System.out.println("Invalid command, try again.");
                    break;
            }
        }
    }
}
