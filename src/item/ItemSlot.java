package item;

public enum ItemSlot {
    head,
    body,
    legs,
    weapon
}
