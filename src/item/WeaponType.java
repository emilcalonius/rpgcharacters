package item;

public enum WeaponType {
    axe,
    bow,
    dagger,
    hammer,
    staff,
    sword,
    wand
}
