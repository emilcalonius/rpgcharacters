package item;

public abstract class Item {
    //Attributes
    private String name;
    // Level requirement to equip item
    private int lvlReq;
    // Slot where the item can be equipped for a character
    private ItemSlot slot;

    // Constructors
    public Item(String name, int lvlReq, ItemSlot slot) {
        this.name = name;
        this.lvlReq = lvlReq;
        this.slot = slot;
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvlReq() {
        return lvlReq;
    }

    public void setLvlReq(int lvlReq) {
        this.lvlReq = lvlReq;
    }

    public ItemSlot getSlot() { return slot; }

    public void setSlot(ItemSlot slot) {
        this.slot = slot;
    }

    // Methods

}
