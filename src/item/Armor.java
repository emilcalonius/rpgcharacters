package item;

public class Armor extends Item {
    // Attributes
    private ArmorType type;
    private int intelligence;
    private int strength;
    private int dexterity;

    // Constructors
    public Armor(String name, int lvlReq, ItemSlot slot, ArmorType type, int intelligence, int strength, int dexterity) {
        super(name, lvlReq, slot);
        this.type = type;
        this.intelligence = intelligence;
        this.strength = strength;
        this.dexterity = dexterity;
    }

    // Getters and setters
    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }
}
