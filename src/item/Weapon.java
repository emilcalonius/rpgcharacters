package item;

public class Weapon extends Item {
    // Attributes
    private WeaponType type;
    // Amount of damage weapon deals per attack
    private int dmg;
    // Attack speed of weapon (attacks per second)
    private double atkSpd;

    // Constructors
    public Weapon(String name, int lvlReq, ItemSlot slot, WeaponType type, int dmg, double atkSpd) {
        super(name, lvlReq, slot);
        this.type = type;
        this.dmg = dmg;
        this.atkSpd = atkSpd;
    }

    // Getters and setters
    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public double getAtkSpd() {
        return atkSpd;
    }

    public void setAtkSpd(double atkSpd) {
        this.atkSpd = atkSpd;
    }

    // Methods
    public double calculateDPS() {
        return dmg * atkSpd;
    }
}
