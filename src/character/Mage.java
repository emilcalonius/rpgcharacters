package character;

import item.*;

import java.util.HashMap;

public class Mage extends Character{
    // Constructors
    // Lvl 1 character with the given name
    public Mage(String name) {
        super(name, 8, 1, 1);
    }

    // Methods
    public void lvlUp() {
        setLvl(getLvl() + 1);
        setIntelligence(getIntelligence() + 5);
        setStrength(getStrength() + 1);
        setDexterity(getDexterity() + 1);
    }

    public Boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getLvlReq() > getLvl()) throw new InvalidWeaponException("Level requirement not met");
        if(weapon.getType() == WeaponType.staff || weapon.getType() == WeaponType.wand) {
            setEquipments(weapon, weapon.getSlot());
            return true;
        } else {
            throw new InvalidWeaponException("Cannot equip weapon of that type");
        }
    }

    public Boolean equipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getLvlReq() > getLvl()) throw new InvalidArmorException("Level requirement not met");
        if(armor.getType() == ArmorType.cloth) {
            setEquipments(armor, armor.getSlot());
            return true;
        } else {
            throw new InvalidArmorException("Cannot equip armor of that type");
        }
    }

    // Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
    public double calculateDPS() {
        Weapon weapon = ((Weapon) getEquipments().get(ItemSlot.weapon));
        double weaponDPS;
        if(weapon == null) {
            weaponDPS = 1;
        } else {
            weaponDPS = weapon.calculateDPS();
        }
        return weaponDPS * (1 + calculateStats().get("intelligence") / 100.0);
    }
}
