package character;

import item.*;

public class Warrior extends Character {
    // Constructors
    // Lvl 1 character with the given name
    public Warrior(String name) {
        super(name, 1, 5, 2);
    }

    // Methods
    public void lvlUp() {
        setLvl(getLvl() + 1);
        setIntelligence(getIntelligence() + 1);
        setStrength(getStrength() + 3);
        setDexterity(getDexterity() + 2);
    }

    public Boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getLvlReq() > getLvl()) throw new InvalidWeaponException("Level requirement not met");
        if(weapon.getType() == WeaponType.axe || weapon.getType() == WeaponType.hammer || weapon.getType() == WeaponType.sword) {
            setEquipments(weapon, weapon.getSlot());
            return true;
        } else {
            throw new InvalidWeaponException("Cannot equip weapon of that type");
        }
    }

    public Boolean equipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getLvlReq() > getLvl()) throw new InvalidArmorException("Level requirement not met");
        if(armor.getType() == ArmorType.mail || armor.getType() == ArmorType.plate) {
            setEquipments(armor, armor.getSlot());
            return true;
        } else {
            throw new InvalidArmorException("Cannot equip armor of that type");
        }
    }

    // Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
    public double calculateDPS() {
        Weapon weapon = ((Weapon) getEquipments().get(ItemSlot.weapon));
        double weaponDPS;
        if(weapon == null) {
            weaponDPS = 1;
        } else {
            weaponDPS = weapon.calculateDPS();
        }
        return weaponDPS * (1 + calculateStats().get("strength") / 100.0);
    }
}
