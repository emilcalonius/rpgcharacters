package character;

import item.*;
import java.util.HashMap;

public abstract class Character {
    // Attributes
    private String name;
    private int lvl;
    // Base attributes of character
    private int intelligence;
    private int strength;
    private int dexterity;
    // Equipments that a character has equipped
    private HashMap<ItemSlot, Item> equipments = new HashMap<>();

    // Constructors
    public Character(String name, int intelligence, int strength, int dexterity) {
        this.name = name;
        this.lvl = 1;
        this.intelligence = intelligence;
        this.strength = strength;
        this.dexterity = dexterity;
        for(ItemSlot slot : ItemSlot.values()) {
            equipments.put(slot, null);
        }
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public HashMap<ItemSlot, Item> getEquipments() {
        return equipments;
    }

    public void setEquipments(Item item, ItemSlot slot) {
        equipments.put(slot, item);
    }

    // Methods
    public HashMap<String, Integer> calculateStats() {
        // Get base attributes
        int intelligence = this.intelligence;
        int strength = this.strength;
        int dexterity = this.dexterity;

        // Loop through equipments and for each armor piece increase attributes accordingly
        for(Item item : equipments.values()) {
            if(item == null || item.getClass() == Weapon.class) continue;
            Armor armor = (Armor) item;
            intelligence += armor.getIntelligence();
            strength += armor.getStrength();
            dexterity += armor.getDexterity();
        }

        // Create hashmap with final values for attributes and return it
        HashMap<String, Integer> stats = new HashMap<>();
        stats.put("intelligence", intelligence);
        stats.put("strength", strength);
        stats.put("dexterity", dexterity);
        return stats;
    }

    public String displayStats() {
        StringBuilder characterSheet = new StringBuilder();
        characterSheet.append("Name: " + this.name + "\n");
        characterSheet.append("Level: " + this.lvl + "\n");

        HashMap<String, Integer> stats = calculateStats();
        characterSheet.append("Strength: " + stats.get("strength") + "\n");
        characterSheet.append("Dexterity: " + stats.get("dexterity") + "\n");
        characterSheet.append("Intelligence: " + stats.get("intelligence") + "\n");
        characterSheet.append("DPS: " + Math.round(calculateDPS() * 100d) / 100d + "\n");

        return characterSheet.toString();
    }

    // Abstract methods
    public abstract void lvlUp();
    public abstract Boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;
    public abstract Boolean equipArmor(Armor armor) throws InvalidArmorException;
    public abstract double calculateDPS();
}
